﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StudentsGrades.DataAccesss
{
    public class Grades : BaseEntity
    {
        public Guid StudentId { get; set; }
        public Guid CourseId { get; set; }
        public DateTime LastChangedDate { get; set; }
        public float Grade { get; set; }
    }
}
