﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StudentsGrades.DataAccesss
{
    public class Course : BaseEntity
    {
        public string CourseName { get; set; }
        public virtual List<Student> CourseStudents { get; set; }
    }
}
