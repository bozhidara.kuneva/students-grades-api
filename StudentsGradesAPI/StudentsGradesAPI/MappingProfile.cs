﻿using AutoMapper;
using StudentsGrades.DataAccesss;
using StudentsGrades.Domain.Queries;

namespace StudentsGradesAPI
{
    public class AutoMapping : Profile
    {

        public AutoMapping()
        {
            CreateMap<GetStudentsResponse, Grade>().ReverseMap()
            .ForMember(dest => dest.StudentName, x => x.MapFrom(src => src.Student.FirstName + " " + src.Student.LastName))
            .ForMember(dest => dest.CourseName, x => x.MapFrom(src => src.Course.CourseName));
        }
    }
}
