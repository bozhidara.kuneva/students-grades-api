﻿using MediatR;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using StudentsGrades.Domain.Commands.CreateStudent;
using StudentsGrades.Domain.Queries.GetStudents;
using System;
using System.Threading.Tasks;


namespace StudentsGradesAPI.Controllers
{
    public class StudentsController : ControllerBase
    {
        private readonly IMediator _mediator;
        public StudentsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [EnableQuery()]
        public async Task<IActionResult> Post([FromBody] CreateStudentCommand request)
        {
            try
            {
                await _mediator.Send(request);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [EnableQuery()]
        public async Task<IActionResult> Get([FromQuery] GetStudentsQuery getStudentsDto)
        {
            try
            {    
               var grades = await _mediator.Send(getStudentsDto);
                return Ok(grades);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
