﻿using MediatR;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using StudentsGrades.Domain.Commands.AddStudentToCourse;
using System;
using System.Threading.Tasks;

namespace StudentsGradesAPI.Controllers
{
    public class GradesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public GradesController(IMediator mediator)
        {
            _mediator = mediator;
        }


        [EnableQuery()]
        public async Task<IActionResult> Post([FromBody] AddStudentGradeToCourseCommand request)
        {
            try
            {
                await _mediator.Send(request);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
