using MediatR;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OData.Edm;
using StudentsGrades.DataAccess;
using StudentsGrades.DataAccesss;
using StudentsGrades.Repositories;
using StudentsGrades.Repositories.Abstraction;
using StudentsGrades.Repositories.Repositories;
using System;
using System.Linq;

namespace StudentsGradesAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region Context
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            #endregion

            services.AddControllers(mvcOptions =>
                 mvcOptions.EnableEndpointRouting = false);

            services.AddOData();
            var assembly = AppDomain.CurrentDomain.Load("StudentsGrades.Domain");

            #region DI
            services.AddScoped<IStudentRepository, StudentRepository>();
            services.AddScoped<ICourseRepository, CourseRepository>();
            services.AddScoped<IGradeRepository, GradeRepository>();
            services.AddMediatR(assembly);
            #endregion

            #region Swagger
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "Students Grades API",
                    Description = "DotNet Core Api 3.1 - with swagger"
                });
            });
            #endregion

            #region AutoMapper
            services.AddAutoMapper(typeof(Startup));
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseMvc(routeBuilder =>
            {
                routeBuilder
                           .Select().Filter().Expand().MaxTop(100)
                           .Count().OrderBy();
                routeBuilder
                .MapODataServiceRoute("odata", "odata", GetEdmModel());
            });

            app.UseSwagger()
               .UseSwaggerUI(c =>
               {
                   c.SwaggerEndpoint("/swagger/v1/swagger.json", "Students Grades API");
               });
        }

        IEdmModel GetEdmModel()
        {
            var odataBuilder = new ODataConventionModelBuilder();
            odataBuilder.EntitySet<Student>("Students");
            odataBuilder.EntitySet<Course>("Courses");
            odataBuilder.EntitySet<Grade>("Grades");

            return odataBuilder.GetEdmModel();
        }
    }
}
