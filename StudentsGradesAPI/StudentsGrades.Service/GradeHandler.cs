﻿using MediatR;
using StudentsGrades.DataAccesss;
using StudentsGrades.Domain.DTOs;
using StudentsGrades.Repositories.Abstraction;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StudentsGrades.Services
{
    public class GradeHandler : IRequestHandler<SetStudentGradeDto, Grade>
    {
        private readonly IGradeRepository _gradeRepository;
        public GradeHandler(IGradeRepository gradeRepository)
        {
            _gradeRepository = gradeRepository;
        }

        public async Task<Grade> Handle(SetStudentGradeDto request, CancellationToken cancellationToken)
        {
            var isStudentAlreadyAddedToCourse = _gradeRepository.GetSingle<Student>(user => user.StudentId == request.StudentId && user.CourseId == request.CourseId);

            if (isStudentAlreadyAddedToCourse != null)
                throw new Exception("Student is already added to this course.");

            var studentGrade = new Grade
            {
                StudentId = request.StudentId,
                CourseId = request.CourseId,
                LastChangedDate = DateTime.Now,
                Id = Guid.NewGuid(),
                GradeValue = request.GradeValue
            };

            _gradeRepository.Create(studentGrade);

            return studentGrade;
        }

        public async Task<IEnumerable<Grade>> Handle(GetStudentsDto request, CancellationToken cancellationToken)
        {
            if (request.IsTakenExam.HasValue && request.IsTakenExam.Value == true)
            {
                var allPassedStudents = _gradeRepository.GetAll(user => user.GradeValue != 0 && user.GradeValue != 2);
                return allPassedStudents;
            }
            else
            {
                var allFailPassedStudents = _gradeRepository.GetAll(user => user.GradeValue == 0 && user.GradeValue == 2);
                return allFailPassedStudents;
            }
        }
    }
}
