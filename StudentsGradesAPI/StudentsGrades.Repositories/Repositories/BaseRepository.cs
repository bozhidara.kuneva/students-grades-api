﻿using Microsoft.EntityFrameworkCore;
using StudentsGrades.Repositories.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace StudentsGrades.Repositories.Repositories
{
    public class BaseRepository<T> : IDisposable, IBaseRepository<T> where T : class
    {
        private DbContext _context;
        public BaseRepository(DbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public T GetSingle<TProperty>(Expression<Func<T, bool>> where,
                                      Expression<Func<T, TProperty>> include = null)
        {
            if (where == null)
                where = x => true;

            if (include != null)
                return _context.Set<T>().Include(include).FirstOrDefault(where);

            return _context.Set<T>().FirstOrDefault(where);
        }

        public IEnumerable<T> GetAll<TProperty>(Expression<Func<T, bool>> where,
                                               string include1 = null,
                                               string include2 = null)
        {
            if (where == null)
                where = x => true;

            var query = _context.Set<T>().AsQueryable();

            if (include1 != null)
                query = query.Include(include1);

            if (include2 != null)
                query = query.Include(include2);

            query = query.Where(where);

            return query.ToList();
        }

        public IEnumerable<T> GetAll(Expression<Func<T, bool>> where)
        {
            if (where == null)
                where = x => true;

            var query = _context.Set<T>().AsQueryable();

            query = query.Where(where);

            return query.ToList();
        }


        public void Create(T entity)
        {
            _context.Set<T>().Add(entity);

            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
