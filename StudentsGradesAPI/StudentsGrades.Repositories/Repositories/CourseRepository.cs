﻿using StudentsGrades.DataAccess;
using StudentsGrades.DataAccesss;
using StudentsGrades.Repositories.Abstraction;

namespace StudentsGrades.Repositories.Repositories
{
    public class CourseRepository : BaseRepository<Course>, ICourseRepository
    {
        public CourseRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}

