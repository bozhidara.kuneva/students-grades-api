﻿using StudentsGrades.DataAccess;
using StudentsGrades.DataAccesss;
using StudentsGrades.Repositories.Abstraction;
using StudentsGrades.Repositories.Repositories;

namespace StudentsGrades.Repositories
{
    public class StudentRepository : BaseRepository<Student>, IStudentRepository
    {
        public StudentRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
