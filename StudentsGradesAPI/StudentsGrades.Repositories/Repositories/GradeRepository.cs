﻿using StudentsGrades.DataAccess;
using StudentsGrades.DataAccesss;
using StudentsGrades.Repositories.Abstraction;

namespace StudentsGrades.Repositories.Repositories
{
    public class GradeRepository : BaseRepository<Grade>, IGradeRepository
    {
        public GradeRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}