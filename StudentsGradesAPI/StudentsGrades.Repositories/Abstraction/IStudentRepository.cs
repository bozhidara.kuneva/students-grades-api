﻿using StudentsGrades.DataAccesss;

namespace StudentsGrades.Repositories.Abstraction
{
    public interface IStudentRepository : IBaseRepository<Student>
    {
    }
}
