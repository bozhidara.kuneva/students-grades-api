﻿using StudentsGrades.DataAccesss;

namespace StudentsGrades.Repositories.Abstraction
{
    public interface ICourseRepository : IBaseRepository<Course>
    {
    }
}
