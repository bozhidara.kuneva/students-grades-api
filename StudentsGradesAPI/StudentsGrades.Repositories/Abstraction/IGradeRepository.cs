﻿using StudentsGrades.DataAccesss;

namespace StudentsGrades.Repositories.Abstraction
{
    public interface IGradeRepository : IBaseRepository<Grade>
    {
    }
}

