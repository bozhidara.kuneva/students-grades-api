﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace StudentsGrades.Repositories.Abstraction
{
    public interface IBaseRepository<T> where T : class
    {
        IEnumerable<T> GetAll<TProperty>(Expression<Func<T, bool>> where, 
                                         string include1 = null,
                                         string include2 = null);
        void Create(T entity);
        T GetSingle<TProperty>(Expression<Func<T, bool>> where,
                               Expression<Func<T, TProperty>> include = null);
    }
}