﻿using AutoMapper;
using MediatR;
using StudentsGrades.DataAccesss;
using StudentsGrades.Domain.Constants;
using StudentsGrades.Repositories.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace StudentsGrades.Domain.Queries.GetStudents
{
    public class GetStudentsHandler : IRequestHandler<GetStudentsQuery, IEnumerable<GetStudentsResponse>>
    {
        private readonly IGradeRepository _gradeRepository;
        private readonly IMapper _mapper;
        public GetStudentsHandler(IGradeRepository gradeRepository, IMapper mapper)
        {
            _gradeRepository = gradeRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<GetStudentsResponse>> Handle(GetStudentsQuery request, CancellationToken cancellationToken)
        {
            Expression<Func<Grade, bool>> whereClause = null;
            if (request.IsTakenExam.HasValue && request.IsTakenExam.Value == true)
                whereClause = grade => grade.GradeValue > 2;

            if (request.IsTakenExam.HasValue && request.IsTakenExam.Value == false)
                whereClause = grade => grade.GradeValue == 2;

            var allPassedStudents = _gradeRepository.GetAll<Grade>(whereClause, CommonConstants.IncludeEntitiestruct.COURSE_TABLE, CommonConstants.IncludeEntitiestruct.STUDENT_TABLE);
            return MapGradeToStudentsReponse(allPassedStudents);
        }

        private List<GetStudentsResponse> MapGradeToStudentsReponse(IEnumerable<Grade> allPassedStudents)
        {
            var getStudentsResponse = new List<GetStudentsResponse>();
            foreach (var passedStudent in allPassedStudents)
            {
                var studentResponse = new GetStudentsResponse();
                _mapper.Map(passedStudent, studentResponse);

                getStudentsResponse.Add(studentResponse);
            }

            return getStudentsResponse;
        }
    }
}
