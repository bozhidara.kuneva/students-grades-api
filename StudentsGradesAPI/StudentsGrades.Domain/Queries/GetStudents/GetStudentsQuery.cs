﻿using MediatR;
using StudentsGrades.DataAccesss;
using System.Collections.Generic;

namespace StudentsGrades.Domain.Queries.GetStudents
{
    public class GetStudentsQuery : IRequest<IEnumerable<GetStudentsResponse>>
    {
        public bool? IsTakenExam { get; set; }
        public string StudentName { get; set; }
    }
}
