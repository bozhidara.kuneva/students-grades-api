﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StudentsGrades.Domain.Queries
{
    public class GetStudentsResponse
    {
        public Guid StudentId { get; set; }
        public Guid CourseId { get; set; }
        public DateTime LastChangedDate { get; set; }
        public float GradeValue { get; set; }
        public string StudentName { get; set; }
        public string CourseName { get; set; }
    }
}
