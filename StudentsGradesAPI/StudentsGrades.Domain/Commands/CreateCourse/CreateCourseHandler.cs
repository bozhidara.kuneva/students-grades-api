﻿using MediatR;
using StudentsGrades.DataAccesss;
using StudentsGrades.Domain.Commands.CreateCourse;
using StudentsGrades.Repositories.Abstraction;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudentsGrades.Domain.Commands.CreateCourse
{
    public class CreateCourseHandler : IRequestHandler<CreateCourseCommand>
    {
        private readonly ICourseRepository _courseRepository;

        public CreateCourseHandler(ICourseRepository courseRepository)
        {
            _courseRepository = courseRepository;
        }

        public async Task<Unit> Handle(CreateCourseCommand request, CancellationToken cancellationToken)
        {
            var course = new Course
            {
                CourseName = request.CourseName,
                Id = Guid.NewGuid(),
            };

            _courseRepository.Create(course);

            return Unit.Value;
        }
    }
}
