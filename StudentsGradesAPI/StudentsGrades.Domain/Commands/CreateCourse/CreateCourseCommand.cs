﻿using MediatR;

namespace StudentsGrades.Domain.Commands.CreateCourse
{
    public class CreateCourseCommand : IRequest
    {
        public string CourseName { get; set; }
    }
}
