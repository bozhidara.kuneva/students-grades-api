﻿using MediatR;
using System;

namespace StudentsGrades.Domain.Commands.AddStudentToCourse
{
    public class AddStudentGradeToCourseCommand : IRequest
    {
        public Guid StudentId { get; set; }
        public Guid CourseId { get; set; }
        public float GradeValue { get; set; }

    }
}
