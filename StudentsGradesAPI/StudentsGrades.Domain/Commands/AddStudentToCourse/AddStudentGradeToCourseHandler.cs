﻿using MediatR;
using StudentsGrades.DataAccesss;
using StudentsGrades.Domain.Commands.AddStudentToCourse;
using StudentsGrades.Repositories.Abstraction;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudentsGrades.Domain.Commands.SetStudentGrade
{
    public class AddStudentGradeToCourseHandler : IRequestHandler<AddStudentGradeToCourseCommand>
    {
        private readonly IGradeRepository _gradeRepository;
        public AddStudentGradeToCourseHandler(IGradeRepository gradeRepository)
        {
            _gradeRepository = gradeRepository;
        }

        public async Task<Unit> Handle(AddStudentGradeToCourseCommand request, CancellationToken cancellationToken)
        {
            var isStudentAlreadyAddedToCourse = _gradeRepository.GetSingle<Student>(user => user.StudentId == request.StudentId && user.CourseId == request.CourseId);

            if (isStudentAlreadyAddedToCourse != null)
                throw new Exception("Student grade has been already added to this course.");

            var studentGrade = new Grade
            {
                StudentId = request.StudentId,
                CourseId = request.CourseId,
                LastChangedDate = DateTime.Now,
                Id = Guid.NewGuid(), 
                GradeValue = request.GradeValue
            };

            _gradeRepository.Create(studentGrade);

            return Unit.Value;
        }
    }
}
