﻿using MediatR;

namespace StudentsGrades.Domain.Commands.CreateStudent
{
    public class CreateStudentCommand : IRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
