﻿using MediatR;
using StudentsGrades.DataAccesss;
using StudentsGrades.Repositories.Abstraction;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudentsGrades.Domain.Commands.CreateStudent
{
    public class CreateStudentHandler : IRequestHandler<CreateStudentCommand>
    {
        private readonly IStudentRepository _studentRepository;

        public CreateStudentHandler(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }
        public async Task<Unit> Handle(CreateStudentCommand request, CancellationToken cancellationToken)
        {
            var student = new Student
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
            };

            _studentRepository.Create(student);

            return Unit.Value;
        }
    }
}
