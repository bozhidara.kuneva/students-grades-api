﻿namespace StudentsGrades.Domain.Constants
{
    public static class CommonConstants
    {
        public struct IncludeEntitiestruct
        {
            public const string COURSE_TABLE = "Course";
            public const string STUDENT_TABLE = "Student";
        }
    }
}
