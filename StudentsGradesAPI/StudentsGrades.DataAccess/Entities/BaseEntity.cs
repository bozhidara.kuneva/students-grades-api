﻿using System;

namespace StudentsGrades.DataAccesss
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
