﻿using System.Collections.Generic;

namespace StudentsGrades.DataAccesss
{
    public class Student : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual ICollection<Grade> StudentCourses { get; set; }
    }
}
