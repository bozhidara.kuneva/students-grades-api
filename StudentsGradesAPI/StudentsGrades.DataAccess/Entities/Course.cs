﻿using System.Collections.Generic;

namespace StudentsGrades.DataAccesss
{
    public class Course : BaseEntity
    {
        public string CourseName { get; set; }
        public virtual ICollection<Grade> StudentCourses { get; set; }
    }
}
