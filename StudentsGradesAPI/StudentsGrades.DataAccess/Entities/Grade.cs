﻿using System;

namespace StudentsGrades.DataAccesss
{
    public class Grade : BaseEntity
    {
        public Guid StudentId { get; set; }
        public Guid CourseId { get; set; }
        public DateTime LastChangedDate { get; set; }
        public float GradeValue { get; set; }
        public  Student Student { get; set; }
        public  Course Course { get; set; }
    }
}
