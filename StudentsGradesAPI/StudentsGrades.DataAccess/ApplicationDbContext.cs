﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using StudentsGrades.DataAccesss;

namespace StudentsGrades.DataAccess
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Grade>()
                .HasKey(bc => new { bc.StudentId, bc.CourseId });
            builder.Entity<Grade>()
                .HasOne(bc => bc.Course)
                .WithMany(b => b.StudentCourses)
                .HasForeignKey(bc => bc.CourseId);
            builder.Entity<Grade>()
                .HasOne(bc => bc.Student)
                .WithMany(c => c.StudentCourses)
                .HasForeignKey(bc => bc.StudentId);
        }
        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Grade> Grades { get; set; }

    }
}
